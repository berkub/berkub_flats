require 'test_helper'

class TicketsControllerTest < ActionController::TestCase
  def setup
    @ticket = tickets(:one)
  end

  def teardown
    @ticket = nil
  end

  test '#create sends notification email' do
    assert_difference 'ActionMailer::Base.deliveries.size', +1 do
      xhr :post, :create, ticket: @ticket.attributes.except(:id)
    end

    notification_email = ActionMailer::Base.deliveries.last

    assert_equal 'adam@berkub.com', notification_email.to[0]
  end
end
