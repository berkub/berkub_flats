require 'test_helper'

class WelcomeControllerTest < ActionController::TestCase
  def setup
    Flat.any_instance.stubs(:cover).returns(photos(:one))
  end

  test '#index responds successfully' do
    get :index
    assert_response :success
  end
end
