require 'test_helper'

class FlatsControllerTest < ActionController::TestCase
  def setup
    @flat = flats(:one)
    sign_in users(:one)

    Flat.any_instance.stubs(:cover).returns(photos(:one))
  end

  def teardown
    @flat = nil
  end

  test '#index responds successfully' do
    get :index
    assert_response :success
  end

  test '#new assigns @flat and redirects to #edit' do
    get :new
    assert_not_nil assigns(:flat)
    assert_redirected_to edit_flat_url(assigns(:flat))
  end

  test '#create creates new flat' do
    assert_difference('Flat.count') do
      post :create, flat: { address: 'Na Slupi 2c', rooms: '1+kk', rent: '18000', utilities: '2000', description: 'Small and nice.' }
    end

    assert_redirected_to flat_url(assigns(:flat))
  end

  test '#edit responds successfully and assigns @flat' do
    get :edit, id: @flat.to_param
    assert_response :success
    assert_not_nil assigns(:flat)
    assert_equal assigns(:flat), @flat
  end

  test '#update updates flat' do
    get :update, id: @flat.to_param, flat: { rent: 20000 }
    assert_equal 20000, @flat.reload.rent
    assert_redirected_to flat_url(@flat)
  end

  test '#show responds successfully and assigns @flat' do
    get :show, id: @flat.to_param
    assert_response :success
    assert_not_nil assigns(:flat)
  end
end
