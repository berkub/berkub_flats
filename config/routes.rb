Rails.application.routes.draw do
  devise_for :users, path: '/', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'signup' }

  namespace :editor do
    resources :flats, except: [:index, :show] do
      post '/share_on_vk', to: 'flats#share_on_vk', on: :member
      post '/feature', to: 'flats#feature', on: :member
      post '/rent', to: 'flats#rent', on: :member
      post '/unpublish', to: 'flats#unpublish', on: :member
    end
  end

  resources :flats do
    get '/match', to: 'flats#mark', on: :member
    get '/share', to: 'flats#share', on: :member
  end

  resources :tickets

  resources :clients
  resources :searches
  resources :requests

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome#index'

  get "/:id", to: "pages#show", as: :page
end
