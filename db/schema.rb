# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150113082822) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "clients", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "availability"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "districts", force: true do |t|
    t.string   "name"
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flat_layouts", force: true do |t|
    t.string   "name"
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "flats", force: true do |t|
    t.string   "address"
    t.float    "rent"
    t.string   "rooms"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "area"
    t.boolean  "published",          default: false
    t.boolean  "utility_included",   default: false
    t.integer  "cover_id"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "part"
    t.text     "description_ru"
    t.boolean  "featured",           default: false
    t.boolean  "rented",             default: false
    t.string   "external_source_id"
    t.integer  "vk_post_id"
    t.integer  "layout_id"
    t.integer  "district_id"
  end

  create_table "jobs", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "open",        default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photos", force: true do |t|
    t.string   "file"
    t.integer  "flat_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "marked",     default: false
  end

  create_table "requests", force: true do |t|
    t.integer  "flat_id"
    t.integer  "client_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.text     "availability"
  end

  create_table "searches", force: true do |t|
    t.float    "rent"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "filters"
    t.string   "part"
    t.string   "rooms"
  end

  create_table "tickets", force: true do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "flat_id"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
