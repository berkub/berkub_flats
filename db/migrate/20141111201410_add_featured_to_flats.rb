class AddFeaturedToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :featured, :boolean, default: false
  end
end
