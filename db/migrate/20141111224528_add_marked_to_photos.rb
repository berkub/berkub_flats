class AddMarkedToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :marked, :boolean, default: false
  end
end
