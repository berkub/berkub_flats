class AddClientDataToRequests < ActiveRecord::Migration
  def change
    add_column :requests, :name, :string
    add_column :requests, :phone, :string
    add_column :requests, :email, :string
    add_column :requests, :availability, :text
  end
end
