class AddExternalSourceIdToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :external_source_id, :string
  end
end
