class AddCoverIdToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :cover_id, :integer
  end
end
