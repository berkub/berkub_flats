class AddFlatIdToTickets < ActiveRecord::Migration
  def change
    add_column :tickets, :flat_id, :integer
  end
end
