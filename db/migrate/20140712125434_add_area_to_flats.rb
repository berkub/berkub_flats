class AddAreaToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :area, :float
  end
end
