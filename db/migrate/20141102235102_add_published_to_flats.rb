class AddPublishedToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :published, :boolean, default: false
  end
end
