class AddUtilityIncludedToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :utility_included, :boolean, default: false
  end
end
