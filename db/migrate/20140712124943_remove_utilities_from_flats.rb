class RemoveUtilitiesFromFlats < ActiveRecord::Migration
  def change
    remove_column :flats, :utilities
  end
end
