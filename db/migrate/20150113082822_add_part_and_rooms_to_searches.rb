class AddPartAndRoomsToSearches < ActiveRecord::Migration
  def change
    add_column :searches, :part, :string unless column_exists? :searches, :part
    add_column :searches, :rooms, :string unless column_exists? :searches, :rooms
  end
end
