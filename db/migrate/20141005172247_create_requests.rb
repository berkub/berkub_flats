class CreateRequests < ActiveRecord::Migration
  def change
    drop_table :requests if table_exists? :requests

    create_table :requests do |t|
      t.integer :flat_id
      t.integer :client_id

      t.timestamps
    end
  end
end
