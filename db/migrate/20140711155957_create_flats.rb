class CreateFlats < ActiveRecord::Migration
  def change
    create_table :flats do |t|
      t.string :address
      t.float :rent
      t.float :utilities
      t.string :rooms
      t.text :description

      t.timestamps
    end
  end
end
