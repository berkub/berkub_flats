class AddRentedToFlats < ActiveRecord::Migration
  def change
    add_column :flats, :rented, :boolean, default: false
  end
end
