namespace :flats do
  namespace :feelhome do
    desc 'Synchronize flats rented attribute according to the mirror page at feelhome.cz'
    task :sync, [:filename] => :environment do |t, args|
      counter = 0
      rows = CSV.open(args[:filename], col_sep: ';').to_a
      rows.shift

      rows.each do |row|
        flat_id = row.first
        external_source_id = row.last.split('/').last.to_i

        puts "flat_id: #{flat_id}"
        puts "ext id: #{external_source_id}"

        if Flat.unscoped.exists?(flat_id)
          flat = Flat.unscoped.find(flat_id)
          flat.update_column(:external_source_id, external_source_id)
          counter += 1
        else
          next
        end
      end

      puts "#{counter} out of #{rows.size} flats have been successfully synchronised."
    end
  end
end