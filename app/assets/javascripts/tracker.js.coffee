jQuery ->
  tracker.setup()

tracker =
  setup: ->
    @bind()

  bind: ->
    $('.js-map-toggle').on 'click', -> mixpanel.track('Flat Page Map Expanded')
    $('.js-contact-link').on 'click', -> mixpanel.track('Contact Us Link Clicked')
    $('#new_request').on 'submit', -> mixpanel.track('Flat Request Submitted')

@tracker = tracker