#= require_tree ./flats

Dropzone.autoDiscover = false

jQuery ->
  flats.setup()

  if $('body').hasClass('flats') and $('body').hasClass('edit')
    window.dropzone = new Dropzone('.js-dropzone',
      paramName: 'flat[photos_attributes][][file]'
      url: "/flats/#{$('form').attr('data-flat-id')}"
      method: 'PATCH'
      headers: 'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
      # uploadMultiple: true
      previewsContainer: '.new-flat__photos'
      previewTemplate: '
        <div class="new-flat__photos__photo">
          <img data-dz-thumbnail>
          <div class="new-flat__photos__photo__progress"><span data-dz-uploadprogress></span></div>
          <div class="new-flat__photos__photo__label">
            <i class="ss-icon" data-dz-remove>delete</i>
            <span data-dz-name></span>
          </div>
        </div>
      '
    )

    dropzone.on 'dragover', (e) -> $('.js-dropzone').addClass('hover')
    dropzone.on 'dragleave', (e) -> $('.js-dropzone').removeClass('hover')
    dropzone.on 'dragend', (e) -> $('.js-dropzone').removeClass('hover')

flats =
  setup: ->
    @bind()

  bind: ->
    @publishOnSave()

    $('.js-share-vk').on 'click', (e) ->
      e.preventDefault()
      share.vk()

  publishOnSave: ->
    $('form.edit_flat > input[type=submit]').on 'click', (e) ->
      e.preventDefault()

      $('input#flat_published').val 't'
      $('form.edit_flat').trigger 'submit'

@flats = flats
