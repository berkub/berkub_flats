jQuery ->
  if $('body').hasClass('flats') and $('body').hasClass('show')
    new EditorPanel($('.js-editor-panel'))

class EditorPanel
  constructor: ($panel) ->
    @attributes =
      $panel: $panel
      flat_id: $panel.attr('data-flat-id')
      $actions: $(['share_on_vk', 'feature', 'rent', 'unpublish'])

    @bind()

  bind: =>
    _this = this

    @attributes.$panel.find('.editor-panel__actions__item[data-action] > a').on 'click', (e) ->
      e.preventDefault()

      currentAction = $(this).parent('.editor-panel__actions__item').attr('data-action')

      if $.inArray(currentAction, _this.attributes.$actions) > -1
        _this.request(currentAction)

  request: (action) =>
    $.post("/editor/flats/#{@attributes.flat_id}/#{action}.json")
    .done (response) ->
      $(".editor-panel__actions__item[data-action ~= #{action}]").addClass('editor-panel__actions__item--disabled')

@EditorPanel = EditorPanel