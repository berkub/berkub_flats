jQuery ->
  window.registerParsley = $('form[data-validate~=parsley]').parsley(
    classHandler:  (el) -> ($ el).closest('div.input')
    errorsWrapper: '<div></div>',
    errorTemplate: '<span class="input-error"></span>'
    trigger: "focusout"
  )