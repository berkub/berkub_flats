share =
  vk: ->
    url = window.location.href
    title = $('meta[name=title]').attr('content')
    description = $('meta[name=description]').attr('content')
    url = "http://vk.com/share.php?url=#{encodeURIComponent(url)}&noparse=false"

    @popUp(url)

  popUp: (url) ->
    window.open(url, '', 'toolbar=0,status=0,width=626,height=436')

@share = share
