jQuery ->
  if ($('body').hasClass('flats') and $('body').hasClass('show')) then maps.setup()

maps =
  setup: ->
    google.maps.event.addDomListener(window, 'load', @initMap)
    @bind()

  initMap: ->
    styles = [{"featureType":"landscape","stylers":[{"hue":"#F1FF00"},{"saturation":-27.4},{"lightness":9.4},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#0099FF"},{"saturation":-20},{"lightness":36.4},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#00FF4F"},{"saturation":0},{"lightness":0},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FFB300"},{"saturation":-38},{"lightness":11.2},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#00B6FF"},{"saturation":4.2},{"lightness":-63.4},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#9FFF00"},{"saturation":0},{"lightness":0},{"gamma":1}]}]
    latLng = new google.maps.LatLng($('#map_canvas').attr('data-lat'), $('#map_canvas').attr('data-lng'))
    mapOptions =
      zoom: 16
      center: latLng
      styles: styles

    map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions)

    marker = new google.maps.Marker(
      position: latLng
      map: map
      title: 'BERKUB'
    )

    $(window).on 'flats:map-resize', ->
      google.maps.event.trigger(map, 'resize')

  bind: ->
    mapCanvas = $('#map_canvas')

    $('.js-map-toggle').on 'click', (e) ->
      e.preventDefault()

      mapCanvas.animate { height: '432px' }, 500, -> $(window).trigger('flats:map-resize')
      $(this).hide()


@maps = maps