jQuery ->
  tickets.setup()

tickets =
  setup: ->
    @bind()

  bind: ->
    $(document).on 'open', '.remodal', -> tickets.setModalHeaderImage($(this))

  setModalHeaderImage: ($modal) ->
    imageUrl = $modal.attr('data-cover-image-url')
    $modal.find('.new-ticket__cover').css 'background-image': "linear-gradient(rgba(251, 252, 253, 0) 0%, rgb(251, 252, 253) 100%), url(#{imageUrl})"
