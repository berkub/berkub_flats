jQuery ->
  languageSelector.setup()
  searchPinner.setup()
  # bestFlats.setup()

languageSelector =
  setup: ->
    @bind()

  bind: ->
    $('.header__language-selector').on 'click', (e) -> $(this).toggleClass('header__language-selector--open')
    $('.header__language-selector__language').on 'click', (e) ->
      $('.header__language-selector__language').removeClass('header__language-selector__language--active')
      $(e.target).addClass('header__language-selector__language--active')

searchPinner =
  setup: ->
    @bind()

  bind: ->
    $(document).on 'scroll', ->
      $body = $('body')
      $search = $('.welcome__search')

      if $body.scrollTop() > 510
        $search.addClass 'welcome__search--pinned'
      else
        $search.removeClass 'welcome__search--pinned'

bestFlats =
  setup: ->
    @bind()
    @setTopOffset()

  bind: ->
    $('.welcome__cover__best-flats').on 'click', (e) ->
      e.preventDefault()

      $('html, body').animate
        scrollTop: $('.welcome__flats').offset().top
      2000

  setTopOffset: ->
    $('.welcome__flats').css 'marginTop': $(window).outerHeight(true)

@languageSelector = languageSelector
@searchPinner = searchPinner
@bestFlats = bestFlats