class Editor::FlatsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_flat

  def share_on_vk
    @flat.share_on_vk

    render nothing: true, status: :ok
  end

  def feature
    @flat.update_column(:featured, true)

    render nothing: true, status: :ok
  end

  def rent
    @flat.update_column(:rented, true)

    render nothing: true, status: :ok
  end

  def unpublish
    @flat.update_column(:published, false)

    render nothing: true, status: :ok
  end

private

  def flat_params
    params.require(:flat).permit(:featured, :rented, :published)
  end

  def set_flat
    @flat = Flat.unscoped.find(params[:id])
  end
end
