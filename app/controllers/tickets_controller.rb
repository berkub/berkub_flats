class TicketsController < ApplicationController
  def new
    @ticket = Ticket.new
  end

  def create
    @ticket = Ticket.new(ticket_params)
    @ticket.save

    TicketMailer.new_ticket_notification(@ticket).deliver
  end

private

  def ticket_params
    params.require(:ticket).permit(:name, :phone, :email, :body, :flat_id)
  end
end
