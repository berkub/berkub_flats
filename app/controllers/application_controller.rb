class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :tracker

  # http_basic_authenticate_with name: 'berkub', password: 'emerald' if Rails.env.production?

  def tracker
    @tracker ||= Mixpanel::Tracker.new('bf6adcef5d33245a96d8a60d847ecbd5')
  end
end
