class WelcomeController < ApplicationController
  def index
    @featured_flats = Flat.unscoped.published.available.limit(4).featured.order('created_at desc')
    @recent_flats = Flat.recent
  end
end
