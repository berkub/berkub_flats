class FlatsController < ApplicationController
  before_action :set_flat, only: [:mark, :share]
  before_action :set_unscoped_flat, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]

  def index
    @flats = Flat.all.page(params[:page])
  end

  def new
    @flat = Flat.new
    @flat.published = false
    @flat.save

    redirect_to edit_flat_path(@flat)
  end

  def create
    @flat = Flat.new(flat_params)

    if @flat.save
      redirect_to flat_url(@flat)
    else
      render action: :new
    end
  end

  def mark
    @flat.photos.each do |photo|
      photo.mark!
    end
  end

  def edit
  end

  def update
    if @flat.update(flat_params)
      respond_to do |format|
        format.html { redirect_to flat_url(@flat) }
        format.json { render json: @flat.to_json }
      end
    else
      respond_to do |format|
        format.html { render action: :edit }
        format.json
      end
    end
  end

  def show
    @request = @flat.requests.build
  end

  def share
    @flat.share_on_vk

    redirect_to flat_url(@flat)
  end

private

  def flat_params
    params.require(:flat).permit(:address, :part, :rooms, :rent, :area, :description, :description_ru, :published, :utility_included, :featured, :rented, :external_source_id, :cover_id, { photos_attributes: [:file] })
  end

  def set_flat
    @flat = Flat.find(params[:id])
  end

  def set_unscoped_flat
    @flat = Flat.unscoped.find(params[:id])
  end
end
