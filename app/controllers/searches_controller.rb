class SearchesController < ApplicationController
  before_action :set_search, only: [:show, :update]

  def create
    @search = Search.create!(search_params)
    redirect_to @search
  end

  def update
    @search.update(search_params)
    redirect_to @search
  end

  def show
  end

private

  def search_params
    params.required(:search).permit(:rooms, :rent, :part)
  end

  def set_search
    @search = Search.find(params[:id])
  end
end
