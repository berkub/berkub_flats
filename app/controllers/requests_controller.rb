class RequestsController < ApplicationController
  def create
    @request = Request.create(request_params)
    RequestMailer.new_request_notification(@request).deliver

    respond_to :js
  end

private

  def request_params
    params.require(:request).permit(:name, :phone, :email, :availability, :flat_id)
  end
end
