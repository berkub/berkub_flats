class FlatMailer < ActionMailer::Base
  default from: 'hello@berkub.com'

  def rented_flats_after_cleanup_notification(rented_flat_ids)
    @flats = Flat.unscoped.where(id: rented_flat_ids)
    mail(to: ['adam@berkub.com', 'iakov@berkub.com'], subject: 'Уже сданные квартиры')
  end
end
