class RequestMailer < ActionMailer::Base
  default from: 'hello@berkub.com'

  def new_request_notification(request)
    @request = request
    mail(to: ['adam@berkub.com', 'iakov@berkub.com'], subject: 'Новая заявка')
  end
end
