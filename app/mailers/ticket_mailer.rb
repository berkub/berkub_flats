class TicketMailer < ActionMailer::Base
  default from: 'hello@berkub.com'

  def new_ticket_notification(ticket)
    @ticket = ticket
    mail(to: ['adam@berkub.com', 'iakov@berkub.com'], subject: 'Новая заявка')
  end
end
