class FlatsAvailabilityCleanupJob
  include SuckerPunch::Job

  workers 3

  def perform
    ActiveRecord::Base.connection_pool.with_connection do
      rented_flat_ids = []

      Flat.all.each do |flat|
        begin
          rented_flat_ids << flat.ensure_availability unless flat.rented?
        rescue StandardError => e
          next
        end
      end

      rented_flat_ids = rented_flat_ids.compact
      FlatMailer.rented_flats_after_cleanup_notification(rented_flat_ids).deliver if rented_flat_ids.any?
    end
  end
end
