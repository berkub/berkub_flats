class Client < ActiveRecord::Base
  has_many :requests
  has_many :flats, through: :requests
end
