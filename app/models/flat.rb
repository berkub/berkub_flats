require 'open-uri'

class Flat < ActiveRecord::Base
  include Shareable

  has_many :photos, dependent: :destroy

  has_many :requests
  has_many :clients, through: :requests

  accepts_nested_attributes_for :photos

  default_scope -> { where(published: true, rented: false).order('created_at desc') }
  scope :published, -> { where(published: true) }
  scope :recent, -> { limit(12) }
  scope :featured, -> { where(featured: true) }
  scope :available, -> { where(rented: false) }

  geocoded_by :location
  after_validation :geocode, if: :address_changed?
  after_validation :geocode, if: :part_changed?

  def self.mark_all_photos
    all.each do |flat|
      flat.mark_photos
    end
  end

  def title
    "#{address}, #{part}"
  end

  def cover
    if cover_id.present? && photos.exists?(cover_id)
      photos.find(cover_id)
    else
      photos.last
    end
  end

  def description
    description_ru.presence ? description_ru : super
  end

  def location
    "#{address}, #{part}"
  end

  def mark_photos
    photos.each do |photo|
      photo.mark!
    end
  end

  def ensure_availability
    return false unless external_source_id.present?

    external_source_page = Nokogiri::HTML(open("http://www.feelhome.cz/pronajem/detail/#{external_source_id}/"))

    if (external_source_page.css('#detailprice').text.strip =~ /pronajato/)
      update_column(:rented, true)
      id
    else
      nil
    end
  end
end
