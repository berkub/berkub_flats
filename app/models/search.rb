class Search < ActiveRecord::Base
  before_save :sanitise_filters

  class << self
    def parts
      (1..10).to_a.map do |n|
        "Praha #{n}"
      end.unshift('Расположение')
    end

    def rooms
      (1..5).to_a.map do |n|
        # ["#{n}+1", "#{n}+kk"]
        ["#{n}+kk"]
      end.flatten.unshift('Комнаты')
    end

    def prices
      prices_hash = { 'Цена' => 0.0 }
      [10, 15, 20, 25, 30, 35, 40].each do |n|
        prices_hash.merge!("до #{ActionController::Base.helpers.number_to_currency(n * 1000, format: '%n', precision: 0)} Kč" => (n * 1000).to_f)
      end
      prices_hash
    end
  end

  def flats
    filters = {
      part: part,
      rooms: rooms,
      rent: rent
    }

    @flats ||= search(filters)
  end

private

  def sanitise_filters
    self.part = nil if part == 'Расположение'
    self.rent = nil if rent.to_f == 0.0
    self.rooms = nil if rooms == 'Комнаты'
  end

  def search(filters = [])
    flats = Flat.all
    flats = flats.where("part ILIKE '%#{filters[:part]}%'") if filters[:part].present?
    flats = flats.where("rooms LIKE '#{filters[:rooms].first}%'") if filters[:rooms].present?
    flats = flats.where("rent <= #{filters[:rent].to_f}") if filters[:rent].present?
    flats
  end
end
