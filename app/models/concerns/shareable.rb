module Shareable
  require 'uri'

  include Rails.application.routes.url_helpers

  extend ActiveSupport::Concern

  GROUP_ID = '62346152'

  def share_on_vk
    cover_photo = upload_cover_to_vk

    response = vk.wall.post({
      owner_id: "-#{GROUP_ID}",
      from_group: 1,
      signed: 0,
      message: self.to_vk_post,
      attachments: cover_photo.first.id
    })

    update_column(:vk_post_id, response.post_id)

    File.delete(cover_image_file)
  end

  def shared_on_vk?
    vk_post_id.present?
  end

  def upload_cover_to_vk
    response = VkontakteApi.upload(url: wall_upload_url, photo: [ cover_image_file.path, 'image/jpeg' ])
    response.gid = GROUP_ID
    vk.photos.save_wall_photo(response)
  end

  def cover_image_file
    file = File.open(cover_image_file_name, 'wb')
    file.write open(cover.file.url).read
    file
  end

  def wall_upload_url
    vk.photos.getWallUploadServer(group_id: GROUP_ID).upload_url
  end

  def cover_image_file_name
    "flat-cover-#{id}.jpg"
  end

  def to_vk_post
    "
      #{title}
      #{rooms}, #{area.round} m2
      #{ActionController::Base.helpers.number_to_currency(rent, format: '%n', precision: 0)} Kč (#{utility_included ? I18n.t('flats.utility_included') : I18n.t('flats.utility_not_included')})
      http://berkub.com/flats/#{to_param}
    ".strip
  end

  def vk
    # VkontakteApi.authorization_url(type: :client, scope: [:photos, :wall, :groups])
    # https://oauth.vk.com/authorize?client_id=4638023&redirect_uri=http%3A%2F%2Fapi.vkontakte.ru%2Fblank.html&response_type=token&scope=photos%2Cwall%2Cgroups
    @vk ||= VkontakteApi::Client.new('35a3d61f6cdaaf18f35b620c42d9ec3b4a3af6f20cead52de74c948750f5baf5c629639b394e1f75a42a3')
  end
end