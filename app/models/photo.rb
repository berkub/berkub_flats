require 'open-uri'

class Photo < ActiveRecord::Base
  belongs_to :flat

  mount_uploader :file, PhotoUploader

  def mark!
    mark ||= MiniMagick::Image.open("#{Rails.public_path}/mark.jpg")
    remote_photo = MiniMagick::Image.open(self.file.url).write("#{Rails.public_path}/photos/photo-#{id}.jpg")
    photo = MiniMagick::Image.open("#{Rails.public_path}/photos/photo-#{id}.jpg")

    marked_photo = photo.composite(mark, 'jpg') do |c|
      c.gravity 'SouthEast'
      c.geometry 'x+15'
    end

    File.delete("#{Rails.public_path}/photos/photo-#{id}.jpg")

    self.file = marked_photo
    self.marked = true
    save!
  end
end
